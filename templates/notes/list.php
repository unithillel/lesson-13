<div class="col-4">
    <div class="card">
        <?php if($this->getPreviewImageUrl()):?>
            <img src="<?=$this->getPreviewImageUrl()?>" class="card-img-top" alt="<?=$this->title?>">
        <?php endif;?>
        <div class="card-body">
            <h5 class="card-title"><?=$this->title?></h5>
            <p class="card-text">(<?=$this->dateCreated?>)</p>
            <a href="/notes/show.php?id=<?=$this->id?>" class="btn btn-primary">Read more</a>
            <a href="/notes/edit.php?id=<?=$this->id?>" class="btn btn-warning">Edit</a>
            <form style="display:inline-block" method="post" action="/notes/delete.php">
                <input type="hidden" name="id" value="<?=$this->id?>">
                <button class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>
</div>