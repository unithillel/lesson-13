<?php
if(empty($_POST['title']) || empty($_POST['body']) || empty($_POST['date_created'] || empty($_POST['id']))){
    header('Location:/');
}
require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";

$id = $_POST['id'];
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['body'], ENT_QUOTES, 'UTF-8');
$dateCreated = htmlspecialchars($_POST['date_created'], ENT_QUOTES, 'UTF-8');

$oldNote = Note::create($id, $db);
$image = $oldNote->getPreviewImage();

if(!empty($_FILES['preview_image']['tmp_name'])){
    try{
        $image = Note::uploadPreviewImage($_FILES['preview_image']);
    }catch(Exception $e){
        die('Error uploading image'. $e->getMessage());
    }
}

$note = new Note($title, $body, $dateCreated, $id, $image);
$note->update($db);
header('Location:/?notification=entry_updated');


