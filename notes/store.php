<?php
if(empty($_POST['title']) || empty($_POST['body'])){
    header('Location:/notes/create.php');
}
require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";

$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['body'], ENT_QUOTES, 'UTF-8');
$image = NULL;
if(!empty($_FILES['preview_image'])){
    try{
        $image = Note::uploadPreviewImage($_FILES['preview_image']);
    }catch(Exception $e){
        die('Error uploading image'. $e->getMessage());
    }
}

$note = new Note($title, $body, null, null, $image);
$note->save($db);
header('Location:/?notification=entry_saved');
