<div class="row d-flex justify-content-center">
    <div class="col-md-6">
        <h1>Edit note:</h1>
        <form action="/notes/update.php" method="post" enctype='multipart/form-data'>
            <input type="hidden" name="id" value="<?=$this->id?>">
            <div class="form-group">
                <label> Title:
                    <input type="text" value="<?=$this->title?>" name="title" class="form-control" 
                    placeholder="Enter title...">
                </label>
            </div>
            <div class="form-group">
                <label> Note text:
                    <textarea name="body" placeholder="Enter note text..." class="form-control"><?=$this->body?></textarea>
                </label>
            </div>
            <div class="form-group">
                <label> Date created:
                    <input type="date" value="<?=$this->dateCreated?>" name="date_created" class="form-control" 
                        placeholder="Enter title...">
                </label>
            </div>
            <div class="form-group">
                <?php if($this->getPreviewImageUrl()):?>
                <p>Current image:</p>
                    <img src="<?=$this->getPreviewImageUrl()?>" alt="<?=$this->getPreviewImage()?>">
                    <br>
                <?php endif;?>
                <label> Upload New image:
                    <input type="file" name="preview_image" class="form-control">
                </label>
            </div>

            <button class="btn btn-primary">Update</button>
        </form>
    </div>
</div>