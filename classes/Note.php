<?php
define('FILE_SYSTEM_UPLOAD_PATH', $_SERVER['DOCUMENT_ROOT'].'/upload_images');
class Note{
    protected $id = 0;
    protected $title = "";
    protected $body = "";
    protected $dateCreated = "";
    protected $previewImage = NULL;
    private $noteStatus;
    static public $number;

    static public function create($id, $db){
        $sql = "SELECT * FROM notes WHERE id = :id;";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $notesArray = $statement->fetchAll();
        return new self($notesArray[0]['title'], $notesArray[0]['body'], $notesArray[0]['date_created'], $notesArray[0]['id'], $notesArray[0]['preview_image']);
    }

    static public function all($db){
        try{
            $sql = "SELECT * FROM notes";
            $responseObject = $db->query($sql);
            $notesArray = $responseObject->fetchAll();
            $notesObjects = [];
            foreach($notesArray as $noteArr){
                $notesObjects[] = new Note($noteArr['title'], $noteArr['body'], $noteArr['date_created'], $noteArr['id'], $noteArr['preview_image']);
            }
            return $notesObjects;
        }catch(Exception $e){
            die('Error getting notes!<br>'.$e->getMessage());
        }
    }


    static public function delete($id, $db){
        try{
            $sql = "DELETE FROM notes WHERE id=:id";
            $statement = $db->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
        }catch(Exception $e){
            die('Error deleting note<br>'.$e->getMessage());
        }
    }

    static public function uploadPreviewImage($uploadedFileArray){
        $fileName = time().'-'.$uploadedFileArray['name'];
        $uploadedFile = FILE_SYSTEM_UPLOAD_PATH . '/' . $fileName;
        if(move_uploaded_file($_FILES['preview_image']['tmp_name'], $uploadedFile)){
            return $fileName;
        }else{
            throw new Exception('Error uploading file');
        }
    }

    public function getBody(){
        return strip_tags($this->body);
    }
    public function getTitle(){
        return $this->title;
    }
    public function getId(){
        return $this->id;
    }
    public function getDateCreated(){
        return $this->dataCreated;
    }

    public function getPreviewImage(){
        return $this->previewImage;
    }

    public function getPreviewImageUrl(){
        return '/upload_images/' . $this->getPreviewImage();
    }

    public function setId($id){
        $this->id = (int)$id;
    }

    public function __construct($title, $body, $dateCreated = null, $id = null, $previewImage = null){
        $this->id = $id;
        $this->title = $title;
        $this->body = $body;
        $this->dateCreated = $dateCreated;
        $this->previewImage = $previewImage;
    }

    public function showTemplate($templateName){
        include $_SERVER['DOCUMENT_ROOT'].'/templates/'.$templateName.'.php';
    }

    public function save(PDO $db){
        try{
            
            $sql = "INSERT INTO notes SET 
            title=:title,
            body=:body,
            preview_image=:preview_image,
            date_created=CURDATE()
            ";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':body', $this->body);
            $statement->bindValue(':preview_image', $this->previewImage);
            $statement->execute();
            
        }catch(Exception $e){
            die('Error creating new note!<br>' . $e->getMessage());
        }
    }

    public function update(PDO $db){
        try{
            $sql = "UPDATE notes SET
                title=:title,
                body=:body,
                date_created=:date_created,
                preview_image=:preview_image
             WHERE id = :id";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':body', $this->body);
            $statement->bindValue(':date_created', $this->dateCreated);
            $statement->bindValue(':id', $this->id);
            $statement->bindValue(':preview_image', $this->previewImage);
            $statement->execute();

        }catch(Exception $e){
            die('Error updating note<br>'. $e->getMessage() );
        }
    }

}