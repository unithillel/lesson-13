<?php
try{
    $db = new PDO('mysql:host=localhost;dbname=notes', 'root' , '1234');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');
}catch(Exception $exception){
    echo 'Database is on maintance';
    echo $exception->getMessage();
    die();
}