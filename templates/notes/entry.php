<div class="row">
    <h1><?= $this->title?></h1>
    
    <?php if($this->getPreviewImageUrl()):?>
        <img src="<?=$this->getPreviewImageUrl()?>" class="img-fluid" alt="<?=$this->title?>">
    <?php endif;?>

    <p><?= $this->getBody()?></p>
    <p><?= $this->dateCreated?></p>
    <a href="/">< Back</a>
</div>  