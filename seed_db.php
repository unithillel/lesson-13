<?php
require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";
$testNotesArray = [
    [
        'title' => "PHP is awesome!",
        'body' => "We love php, learn and do!"
    ],
    [
        'title' => "Javascript is awesome!",
        'body' => "We love Javascript, learn and do!"
    ],
    [
        'title' => "CSS is awesome!",
        'body' => "We love CSS, learn and do!"
    ],
];

try{
    foreach($testNotesArray as $tNote){
        $note = new Note($tNote['title'], $tNote['body']);
        $note->save($db);
    }
}catch(Exception $e){
    die("Error while adding Test data.<br>".$e->getMessage());
}
echo "DATABASE Table notes was filled with 3 entries";
