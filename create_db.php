<?php
require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";

try{
    $sql = 'CREATE TABLE notes (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255),
        body TEXT,
        date_created DATE NOT NULL
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
    $db->exec($sql);
}catch(Exception $excptn){
    echo 'Error creating TABLE: notes<br>';
    echo $excptn->getMessage();
    die();
}
echo 'TABLE notes created succesfully!';
die();
?>