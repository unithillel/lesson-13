<?php
require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";

try{

    if(empty($_GET['id'])){
        header('Location:/');
    }
    $id = (int)$_GET['id'];
    $note = Note::create($id, $db);

}catch(Exception $e){
    die('Error getting notes!<br>'.$e->getMessage());
}

?>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
    <?php $note->showTemplate('notes/edit')?>  
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>

