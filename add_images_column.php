<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/database/connect.php';
try{
    $sql = "ALTER TABLE notes ADD preview_image VARCHAR(255)";
    $db->exec("ALTER TABLE notes ADD preview_image VARCHAR(255)");
}catch(Exception $e){
    die('Error adding field <br>'. $e->getMessage());
}
