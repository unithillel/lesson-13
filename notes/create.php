<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-md-6">
            <h1>Create new note:</h1>
            <form method="post" action="/notes/store.php" enctype='multipart/form-data'>
                <div class="form-group">
                    <label for="title">Note title:</label>
                    <input name="title" type="text" class="form-control" id="title">
                </div>

                <div class="form-group">
                    <label for="body">Note Text:</label>
                    <textarea name="body" id="body" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label>
                        Preview Image:
                        <input type="file" name="preview_image" class="form-control">
                    </label>
                </div>

                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>