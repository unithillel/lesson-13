<?php


require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";

$notesObjects = Note::all($db);

?>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
    <div class="row">
        <?php if(!empty($_GET['notification'])):?>
            <?php if($_GET['notification'] == 'entry_saved'):?>
                <div class="alert alert-success" role="alert">
                    Note has been saved succesfully!
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="row">
        <?php foreach($notesObjects as $note):?>
            <?php $note->showTemplate('notes/list')?>
        <?php endforeach;?>
    </div>
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>


