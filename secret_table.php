<?php


require_once $_SERVER['DOCUMENT_ROOT']."/database/connect.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Note.php";

Note::$number=1;
Note::$number++;
Note::$number++;
echo Note::$number;

try{
    $sql = "SELECT * FROM notes";
    $responseObject = $db->query($sql);
    $notesArray = $responseObject->fetchAll();
    $notesObjects = [];
    foreach($notesArray as $noteArr){
        $notesObjects[] = new Note($noteArr['title'], $noteArr['body'], $noteArr['date_created'], $noteArr['id']);
    }
}catch(Exception $e){
    die('Error getting notes!<br>'.$e->getMessage());
}

?>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
   
    <div class="row">
        <table class="table">
            <tr>
                <th>id</th>
                <th>title</th>
                <th>body</th>
            </tr>

            <?php foreach($notesObjects as $note):?>
                <tr>
                    <td><?=$note->getId()?></td>
                    <td><?=$note->getTitle()?></td>
                    <td><?=$note->getBody()?></td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>


